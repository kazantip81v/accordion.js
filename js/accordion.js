var Accordion = function (rootElement) {
	this.rootElement = rootElement;
	this.accordsElement = null;
	this.headerItems = [];
	this.headerItemsIcon = [];
	this.contentItems = [];

	this.handleAccordClick = this.handleAccordClick.bind(this);
};

Accordion.prototype.classNames = {
	headerItemActive: 'active',
	contentItemActive: 'active'
};

Accordion.prototype.toArray = function (collection) {
	return [].slice.call(collection);
};

Accordion.prototype.collectionElements = function () {
	this.accordsElement = this.rootElement.querySelector('.accordion');
	this.headerItems = this.toArray(this.rootElement.querySelectorAll('.accordion__header'));
	this.headerItemsIcon = this.toArray(this.rootElement.querySelectorAll('.accordion__header-icon'));
	this.contentItems = this.toArray(this.rootElement.querySelectorAll('.accordion__content'));

	return this;
};

Accordion.prototype.setActive = function (element, content) {
	element.classList.add(this.classNames.headerItemActive);
	content.classList.add(this.classNames.contentItemActive);

	return this;
};

Accordion.prototype.unsetActive = function (element, content) {
	element.classList.remove(this.classNames.headerItemActive);
	content.classList.remove(this.classNames.contentItemActive);

	return this;
};

Accordion.prototype.handleAccordClick = function (event) {
	this.headerItems.forEach(function (header, index) {
		if (header !== event.target) {
			this.unsetActive(header, this.contentItems[index]);
		} else {
			this.setActive(header, this.contentItems[index]);
		}
	}.bind(this));

	this.headerItemsIcon.forEach(function (icon, index) {
		if (icon === event.target) {
			icon.parentNode.click();
		}
	});
};

Accordion.prototype.delegateEvents = function () {
	this.headerItems.forEach(function (item, i, arr) {
		item.addEventListener('click', this.handleAccordClick);
	}.bind(this));

	return this;
};

Accordion.prototype.render = function () {
	return this.collectionElements().delegateEvents();
};
