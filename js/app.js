var accordionNodes = [].slice.call(document.querySelectorAll('.content'));
var accordionInstances = accordionNodes.map(function (node) {
  return new Accordion(node).render();
});